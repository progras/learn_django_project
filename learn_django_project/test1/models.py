from django.db import models
from sys import stdout
from django.contrib.auth.models import User
import datetime


class Cinema(models.Model):
    name = models.TextField()
    address = models.TextField()

    def __str__(self):
        return self.name + " " + self.address


class Country(models.Model):
    country = models.TextField()

    def __str__(self):
        return self.country


class Genre(models.Model):
    genre = models.TextField()

    def __str__(self):
        return self.genre


class Film(models.Model):
    name = models.TextField()
    year = models.IntegerField()
    counrty = models.ForeignKey(Country)
    #genre = models.ForeignKey(Genre)
    genre = models.ManyToManyField(Genre)


    def __str__(self):
        return self.name


class Show(models.Model):
    datetime = models.DateTimeField()
    cinema = models.ForeignKey(Cinema)
    film = models.ForeignKey(Film)

    def __str__(self):
        return self.cinema.name + " " + self.film.name + " " + str(self.datetime)

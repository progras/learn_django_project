from django.contrib import admin
from test1.models import Cinema, Show, Country, Genre, Film

admin.site.register(Cinema)
admin.site.register(Show)
admin.site.register(Country)
admin.site.register(Genre)
admin.site.register(Film)

# Register your models here.
